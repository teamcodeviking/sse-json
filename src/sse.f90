!*==SSE.spg  processed by SPAG 6.70Dc at 22:05 on 26 Jan 2013
!**
      PROGRAM SSE
!-------------------------------------------------------------c
!
!     Evolves a single star.
!     Mass loss is an option.
!     The timestep is not constant but determined by certain criteria.
!
!     Written by Jarrod Hurley 26/08/97 at the Institute of
!     Astronomy, Cambridge.
!
!-------------------------------------------------------------c
!
!     STELLAR TYPES - KW
!
!        0 - deeply or fully convective low mass MS star
!        1 - Main Sequence star
!        2 - Hertzsprung Gap
!        3 - First Giant Branch
!        4 - Core Helium Burning
!        5 - First Asymptotic Giant Branch
!        6 - Second Asymptotic Giant Branch
!        7 - Main Sequence Naked Helium star
!        8 - Hertzsprung Gap Naked Helium star
!        9 - Giant Branch Naked Helium star
!       10 - Helium White Dwarf
!       11 - Carbon/Oxygen White Dwarf
!       12 - Oxygen/Neon White Dwarf
!       13 - Neutron Star
!       14 - Black Hole
!       15 - Massless Supernova
!
!-------------------------------------------------------------c
      IMPLICIT NONE
!*--SSE36
!
      INCLUDE 'const_bse.h'
!
      INTEGER kw , j , k
!
      REAL*8 i_mass
      REAL*8 mass , mt , z , zpars(20)
      REAL*8 epoch , tms , tphys , tphysf , dtp
      REAL*8 r , lum , ospin
      REAL*8 mc , rc , menv , renv
      CHARACTER*200 text1
      CHARACTER*30 label(16)
      CHARACTER*200 inputfile
      CHARACTER*200 outputfile
      DATA label/' Low Mass MS Star ' , ' Main sequence Star ' ,        &
          &' Hertzsprung Gap ' , ' Giant Branch ' ,                     &
          &' Core Helium Burning ' , ' First AGB ' , ' Second AGB ' ,   &
          &' Naked Helium MS ' , ' Naked Helium HG ' ,                  &
          &' Naked Helium GB ' , ' Helium WD ' , ' Carbon/Oxygen WD ' , &
          &' Oxygen/Neon WD ' , ' Neutron Star ' , ' Black Hole ' ,     &
          &' Massless Supernova '/
      character(*), parameter :: version_text = '0.6.2'
!
!***********************************************************************
! Input:
!
! mass is in solar units.
! z is metallicity in the range 0.0001 -> 0.03 where 0.02 is Population I.
! tphysf is the maximum evolution time in Myr.
!
! neta is the Reimers mass-loss coefficent (neta*4x10^-13; 0.5 normally).
! bwind is the binary enhanced mass loss parameter (inactive for single).
! hewind is a helium star mass loss factor (1.0 normally).
! sigma is the dispersion in the Maxwellian for the SN kick speed (190 km/s).
!
! ifflag > 0 uses WD IFMR of HPE, 1995, MNRAS, 272, 800 (0).
! wdflag > 0 uses modified-Mestel cooling for WDs (0).
! bhflag > 0 allows velocity kick at BH formation (0).
! nsflag > 0 takes NS/BH mass from Belczynski et al. 2002, ApJ, 572, 407 (1).
! mxns is the maximum NS mass (1.8, nsflag=0; 3.0, nsflag=1).
! idum is the random number seed used in the kick routine.
!
! Next come the parameters that determine the timesteps chosen in each
! evolution phase:
!                 pts1 - MS                  (0.05)
!                 pts2 - GB, CHeB, AGB, HeGB (0.01)
!                 pts3 - HG, HeMS            (0.02)
! as decimal fractions of the time taken in that phase.
!
! If you enter a negative mass then parameters for an evolved star are
! required in the order of:
! initial mass, current mass, type, current time & epoch,
! otherwise the star will start on the ZAMS.
!
      CALL GETARG(1, inputfile)
      CALL GETARG(2, outputfile)


      OPEN (22,FILE=inputfile,STATUS='old')
      READ (22,*) mass , z , tphysf
      READ (22,*) neta , bwind , hewind , sigma
      READ (22,*) ifflag , wdflag , bhflag , nsflag , mxns
      READ (22,*) pts1 , pts2 , pts3
!
!***********************************************************************
!
! Set parameters which depend on the metallicity
!
      CALL ZCNSTS(z,zpars)
      IF ( idum.GT.0 ) idum = -idum
!
      IF ( mass.GT.0.0 ) THEN
!
! Initialize the star to begin on the ZAMS.
!
         mt = mass
         kw = 1
         tphys = 0.D0
         epoch = 0.D0
      ELSE
!
! Obtain parameters of evolved star.
!
         READ (22,*) mass , mt , kw , tphys , epoch
      ENDIF
      CLOSE (22)
      WRITE (*,*)
      i_mass = mass
!
! Set the initial spin of the star. If ospin is less than or equal to
! zero at time zero then evolv1 will set an appropriate ZAMS spin. If
! ospin is greater than zero then it will start with that spin regardless
! of the time. If you want to start at time zero with negligible spin
! then I suggest using a negligible value (but greater than 0.001).
!
      ospin = 0
!
! Set the data-save parameter. If dtp is zero then the parameters of the
! star will be stored in the scm array at each timestep otherwise they
! will be stored at intervals of dtp. Setting dtp equal to tphysf will
! store data only at the start and end while a value of dtp greater than
! tphysf will mean that no data is stored.
!
      dtp = 0.D0
!
      CALL EVOLV1(kw,mass,mt,r,lum,mc,rc,menv,renv,ospin,epoch,tms,     &
                & tphys,tphysf,dtp,z,zpars)
!
!***********************************************************************
! Output:
!
      j = 0
      IF ( SCM(1,1) >= 0.0 ) THEN
!
! The scm array stores the stellar parameters at the specified output
! times. The parameters are (in order of storage):
!
!    Time, stellar type, initial mass, currentch mass, log10(L), log10(r),
!    log10(Teff), core mass, epoch and spin.
!
         OPEN (23,FILE=outputfile,STATUS='unknown')
!          text1 = "# t(Myr), type, M0, M, log10(L), log10(R), log10(Teff), Mc, Menv, log10(Rc), log10(Renv), epoch, spin"
!          WRITE (23,'(a)') text1
         WRITE (23, '(a, a, a)')  '{"version": "', version_text, '",'
         WRITE (23, '(a, F12.6, a)') '"mass": ', i_mass, ","
         WRITE (23, '(a, F12.6, a)') '"Z": ', z, ","
         WRITE (23, '(a)') '"track": ['
 50      j = j + 1
         IF ( SCM(j,1) < 0.0 ) THEN
            SCM(j-1,1) = SCM(j,1)
            j = j - 1
         ENDIF
         WRITE (23,99001,advance='no') (SCM(j,1)), (NINT(SCM(j,2))) ,                &
                        & (SCM(j,k),k=3,8), (SCM(j,10)), (SCM(j,9)),(SCM(j,11)),(SCM(j,k),k=12,13)
99001    FORMAT ('[ ', E30.22,', ', i8,', ', E30.22,', ', E30.22,', ', E30.22,', ', E30.22,', ', E30.22,', ', &
& E30.22,', ', E30.22,', ', E30.22,', ', E30.22,', ', E30.22,', ', E30.22,']')

         IF ( SCM(j,1) >= 0.0 ) THEN
             WRITE (23,'(a)') (',')
             GOTO 50
         ENDIF
         WRITE (23,'(a)') (']}')
         CLOSE (23)

       ENDIF
      END
!**
