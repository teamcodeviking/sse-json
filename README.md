sse-json
===

Slightly modified version of Brian Hurley's SSE program for generating 
stellar evolution tracks.  This version produces json output, making it 
easier to use in other programs.  It is intended to be used with the 
[sse-proc](https://bitbucket.org/teamcodeviking/sse-proc) python package.


Build and install instructions
-------------------

    cmake .
    make
    sudo make install

